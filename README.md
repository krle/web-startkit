# WEB startkit

Prerequisites:

* tmux
* vex
* python
* pip
* node
* yarn

After clone you should initialize and run dev servers:

```
# bin/init.sh
# bin/dev.sh
```
